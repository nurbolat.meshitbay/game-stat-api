<?php

namespace Tests\Unit;

use App\Http\Controllers\ResultController;
use App\Http\Requests\Result\GetTopTenRequest;
use App\Http\Requests\Result\StoreResultRequest;
use App\Http\Resources\ResultResource;
use App\Services\ResultService;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;

class ResultControllerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testStoreResult()
    {
        $mockRequest = $this->createMock(StoreResultRequest::class);
        $mockService = $this->createMock(ResultService::class);

        $mockService->expects($this->once())->method('storeResult');

        $controller = new ResultController($mockService);
        $response = $controller->storeResult($mockRequest);

        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * @throws Exception
     */
    public function testGetTopTen()
    {
        $mockRequest = $this->createMock(GetTopTenRequest::class);
        $mockService = $this->createMock(ResultService::class);

        $mockRequest->expects($this->once())->method('validated')->willReturn([]);
        $mockService->expects($this->once())->method('getTopTen')->willReturn([]);

        $controller = new ResultController($mockService);
        $response = $controller->getTopTen($mockRequest);

        $this->assertInstanceOf(ResultResource::class, $response);
    }
}

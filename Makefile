#enter to task-app container
api:
	docker exec -it task-api bash

#enter to task-db container
db:
	docker exec -it task-db bash

#enter to task-nginx container
nginx:
	docker exec -it task-nginx bash

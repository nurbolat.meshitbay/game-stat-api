<?php

use App\Http\Controllers\ResultController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'result'], function () {
    Route::post('/', [ResultController::class, 'storeResult']);
    Route::get('/top-ten', [ResultController::class, 'getTopTen']);
});


<?php

namespace App\DTO;

class StoreResultDto
{
    public function __construct(
        public int     $milliseconds,
        public ?string $email,
        public ?int    $memberId
    )
    {
    }
}

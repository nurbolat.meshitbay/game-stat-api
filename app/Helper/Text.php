<?php

namespace App\Helper;

use Illuminate\Database\Eloquent\Collection;

class Text
{
    public static function maskHalfOfTitle(string $title): string
    {
        $emailParts = explode('@', $title);
        $localPart = $emailParts[0];
        $domainPart = '@' . $emailParts[1];

        $length = strlen($localPart);
        $halfLength = ceil($length / 2);
        $maskedPart = str_repeat('*', $halfLength);

        return substr($localPart, 0, $length - $halfLength) . $maskedPart . $domainPart;
    }
}

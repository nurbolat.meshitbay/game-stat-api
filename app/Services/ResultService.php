<?php

namespace App\Services;

use App\DTO\StoreResultDto;
use App\Repositories\MemberRepository;
use App\Repositories\ResultRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection as SupportCollection;
use App\Helper\Text;

class ResultService
{
    public function storeResult(StoreResultDto $dto): void
    {
        try {
            $memberId = null;

            if ($dto->email !== null) {
               $memberId = $this->getMemberId($dto->email);
            }

            $dto->memberId = $memberId;

            ResultRepository::store($dto);
        } catch (\Exception $exception) {
            Log::info('Detected error. Content: ' . $exception->getMessage());
        }
    }

    private function getMemberId(string $email): ?int
    {
        if (MemberRepository::isExistsByEmail($email)) {
            $member = MemberRepository::getByEmail($email);
        } else {
            $member = MemberRepository::create($email);
        }

       return $member->id;
    }

    public function getTopTen(array $params, ResultRepository $resultRepository): array
    {
        $selfResult = null;
        $email = Arr::get($params, 'email');
        $topTenResults = $resultRepository->getTopLimitedResults();

        if ($email !== null) {
            $selfResult = $this->getSelfResult($email, $resultRepository);
        }

        return [
                'top' => $this->maskEmails($topTenResults),
                'self' => $selfResult
        ];
    }

    private function getSelfResult(string $email, ResultRepository $resultRepository): object
    {
        return $resultRepository->getResultByEmail($email);
    }

    private function maskEmails(Collection $topResults): Collection
    {
        foreach ($topResults as $each) {
            $each->email = Text::maskHalfOfTitle($each->email);
        }

        return $topResults;
    }
}

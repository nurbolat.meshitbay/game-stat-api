<?php

namespace App\Http\Requests\Result;

use App\DTO\StoreResultDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoreResultRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'milliseconds' => ['required', 'integer'],
            'email' => ['nullable', 'email']
        ];
    }

    public function getDto(): StoreResultDto
    {
        $payload = $this->validated();

        return new StoreResultDto(
            milliseconds: $payload['milliseconds'],
            email: Arr::get($payload, 'email'),
            memberId: null
        );
    }
}

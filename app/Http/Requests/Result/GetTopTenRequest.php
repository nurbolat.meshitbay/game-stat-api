<?php

namespace App\Http\Requests\Result;

use Illuminate\Foundation\Http\FormRequest;

class GetTopTenRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => ['nullable', 'string', 'email', 'exists:members,email']
        ];
    }
}

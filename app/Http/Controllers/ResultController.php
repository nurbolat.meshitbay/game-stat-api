<?php

namespace App\Http\Controllers;

use AllowDynamicProperties;
use App\Http\Requests\Result\GetTopTenRequest;
use App\Http\Requests\Result\StoreResultRequest;
use App\Http\Resources\ResultResource;
use App\Repositories\ResultRepository;
use App\Services\ResultService;
use Illuminate\Http\JsonResponse;

#[AllowDynamicProperties] class ResultController extends Controller
{
    public function __construct(ResultService $resultService)
    {
        $this->resultService = $resultService;
    }

    /**
     * @OA\Post(
     *     path="/api/result",
     *     summary="Сохранение результата игрока",
     *     description="Сохраняет результат игрока в системе.",
     *     operationId="storeResult",
     *     tags={"Results"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Данные для сохранения результата",
     *         @OA\JsonContent(
     *             required={"milliseconds"},
     *             @OA\Property(property="email", type="string", format="email", example="user@example.com"),
     *             @OA\Property(property="milliseconds", type="integer", example=123456)
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Результат успешно сохранен"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Ошибка валидации"
     *     )
     * )
     */

    public function storeResult(StoreResultRequest $request): JsonResponse
    {
        $dto = $request->getDto();
        $this->resultService->storeResult($dto);

        return response()->json(null, 204);
    }

    /**
     * @OA\Get(
     *     path="/api/result/top-ten",
     *     summary="Получение топ 10 результатов",
     *     description="Возвращает топ 10 результатов игры.",
     *     operationId="getTopTen",
     *     tags={"Results"},
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="Email игрока для получения его результата",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешный ответ",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Ошибка валидации"
     *     )
     * )
     */
    public function getTopTen(GetTopTenRequest $request): ResultResource
    {
        $this->resultRepository = new ResultRepository();

        return new ResultResource(
            $this->resultService->getTopTen($request->validated(), $this->resultRepository)
        );
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ResultResource extends JsonResource
{
    public static $wrap = 'data';

    public function toArray(Request $request): array
    {
        return [
                'top' => $this->resource['top'],
                'self' => $this->whenNotNull($this->resource['self'])
        ];
    }
}

<?php

namespace App\Repositories;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;

class MemberRepository
{
    public static function create(string $email): Model
    {
        return Member::query()->create([
           'email' => $email
        ]);
    }

    public  static function isExistsByEmail(string $email): bool
    {
        return Member::query()
            ->where('email', '=', $email)
            ->exists();
    }

    public static function getByEmail(string $email): Model
    {
        return Member::query()
            ->where('email', '=', $email)
            ->first();
    }
}

<?php

namespace App\Repositories;

use App\DTO\StoreResultDto;
use App\Models\Result;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ResultRepository
{
    public static function store(StoreResultDto $dto): void
    {
        Result::query()->create([
            'member_id' => $dto->memberId,
            'milliseconds' => $dto->milliseconds
        ]);
    }

    public function getTopLimitedResults(int $count = 10): Collection
    {
        return $this->getTopResultsQuery()
            ->take(10)
            ->get();
    }

    public function getRankByEmail(string $email)
    {

    }

    private function getTopResultsQuery(): Builder
    {
        return Result::query()
            ->selectRaw("email, milliseconds, RANK() OVER (ORDER BY milliseconds) as position")
            ->fromSub(function ($query) {
                $query->from('results')
                    ->selectRaw('members.email, MIN(results.milliseconds) as milliseconds')
                    ->join('members', 'results.member_id', '=', 'members.id')
                    ->groupBy('members.email');
            }, 'best_results');
    }

    public function getResultByEmail(string $email): object
    {
        return DB::query()
            ->select(['email', 'milliseconds', 'position'])
            ->fromSub($this->getTopResultsQuery(), 'best_results')
            ->where('email', '=', $email)
            ->first();
    }
}

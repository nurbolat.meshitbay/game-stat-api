
## Тестовое задание. Следуйте по шагам:

1. Спульте проект.

2. docker compose build

3. docker compose up -d

4. В корне проекта запустите команду 'sh installation-api.sh'

5. Далее, можете обращаться через swagger (localhost:97/api/documentation) или через Postman Api Collection (https://api.postman.com/collections/29113744-a0058d9b-b677-4b69-991f-bea0b686cf8c?access_key=PMAT-01HKTD0QQZS5R957R5HF6JPJ5S).

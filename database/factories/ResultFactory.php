<?php

namespace Database\Factories;

use App\Models\Member;
use App\Models\Result;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResultFactory extends Factory
{
    protected $model = Result::class;

    public function definition(): array
    {
        return [
            'member_id' => null,
            'milliseconds' => $this->faker->numberBetween(1000, 10000)
        ];
    }
}
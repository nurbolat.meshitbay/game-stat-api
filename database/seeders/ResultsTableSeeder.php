<?php

namespace Database\Seeders;

use App\Models\Member;
use App\Models\Result;
use Illuminate\Database\Seeder;

class ResultsTableSeeder extends Seeder
{
    public function run()
    {
        Member::factory()->count(10000)->create()->each(function ($member) {
            Result::factory()->create([
                'member_id' => $member->id,
                'milliseconds' => rand(100, 9999)
            ]);
            sleep(0.5);
        });
    }
}
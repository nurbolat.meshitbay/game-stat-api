docker exec task-api cp .env.example .env
docker exec task-api composer install
docker exec task-api php artisan l5-swagger:generate
docker exec task-api php artisan key:generate
docker exec task-api php artisan migrate
docker exec task-api php artisan db:seed --class=ResultsTableSeeder

